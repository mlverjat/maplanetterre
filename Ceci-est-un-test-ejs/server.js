// load the things we need
var express = require('express');
var app = express();
var mysql = require('mysql');
var bodyparser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');

// set the view engine to ejs
app.set('view engine', 'ejs');

// using app.use to serve up static CSS files in public/assets/ folder when /public link is called in ejs files
// app.use("/route", express.static("foldername"));
app.use(express.static('public'));


app.use(bodyparser.json());
//var urlencodedparser = bodyparser.urlencoded({ extended: true });
//app.use(urlencodedparser);

// Connection base de donnée 

var connexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Simplon007!',
    database: 'appliecolo'
});

var logged_user = "";
var message = "";

connexion.connect();

// use res.render to load up an ejs view file

// index page 
app.get('/', function(req, res) {
    res.render('pages/index');
});
// creacompteuser page
app.get('/creacompteuser', function (req,res) {
    res.render('pages/creacompteuser');
});

//page défi
app.get('/defi', function (req, res) {
	res.render('pages/defi');
});

//page défi
app.get('/defiliste', function (req, res) {
	res.render('pages/defiliste');
});

//page admin
app.get('/admin', function (req, res) {
	res.render('pages/admin');
});


// ajoutdefiuser page 
app.get('/ajoutdefiuser', function(req, res) {
    res.render('pages/ajoutdefiuser');
});

// nouvel utilisateur page user_add
app.get('/user_add', function (req,res) {
    res.render('pages/user_add');
});

//creation nouveau compte utilisateur
//app.post("pages/user_add", urlencodedparser, function (req, res) {
app.post("/user/add", function (req, res) {
    
    // On vérifie si on a déjà un compte liée a cette adresse mail
    var select = "SELECT nom, mail, password FROM user WHERE mail = (?);";
    var mail = [req.body.email];
    select = mysql.format(select, mail); //mail lié au back , a ma bdd
    connexion.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déjà utilisée!")
        }
    });
    // On insère le nouveau compte en bdd
    var sql = "INSERT INTO user (nom, mail, password) VALUES (?, ?, ?);"
    var values = [req.body.pseudo, req.body.email, bcrypt.hashSync(req.body.password, null, null)];
    sql = mysql.format(sql, values);
    connexion.query(sql, function (error, results, filelds) {
        if (!error) {
            message = "Votre compte a été créé avec succès!";
            res.redirect("/");
        } else {
            console.log(error);
            res.send(error);
        }
    })
});









app.listen(8080);
console.log('8080 est le port magique');

